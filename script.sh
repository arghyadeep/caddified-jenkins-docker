#!/bin/sh

cat > Caddyfile <<EOF
$1
reverse_proxy 127.0.0.1:8080
EOF

caddy start
/sbin/tini -- /usr/local/bin/jenkins.sh
